package pl.edu.pwr.pp;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.stream.IntStream;

public class ImageConverter {

	/**
	 * Znaki odpowiadające kolejnym poziomom odcieni szarości - od czarnego (0)
	 * do białego (255).
	 */
	public static String INTENSITY_2_ASCII_10 = "@%#*+=-:. ";
	public static String INTENSITY_2_ASCII_70 = 
			"$@B%8&WM#*oahkbdpqwmZO0QLCJUYXzcvunxrjft/\\|()1{}[]?-_+~<>i!lI;:,\"^`'. ";


	/**
	 * Metoda zwraca znak odpowiadający danemu odcieniowi szarości. Odcienie
	 * szarości mogą przyjmować wartości z zakresu [0,255]. Zakres jest dzielony
	 * na równe przedziały, liczba przedziałów jest równa ilości znaków w
	 * {@value #INTENSITY_2_ASCII}. Zwracany znak jest znakiem dla przedziału,
	 * do którego należy zadany odcień szarości.
	 * 
	 * 
	 * @param intensity
	 *            odcień szarości w zakresie od 0 do 255
	 * @return znak odpowiadający zadanemu odcieniowi szarości
	 */
	static char intensityToAscii(int intensity, int max_intensity) {
		float number_of_sections = INTENSITY_2_ASCII_10.length();
		float section_length = (max_intensity+1)/number_of_sections;
		int ascii_pos = (int) (intensity/section_length);
		return INTENSITY_2_ASCII_10.charAt(ascii_pos);
	}
	
	static char intensityToAscii_hq(int intensity, int max_intensity) {
		float number_of_sections = INTENSITY_2_ASCII_70.length();
		float section_length = (max_intensity+1)/number_of_sections;
		int ascii_pos = (int) (intensity/section_length);
		return INTENSITY_2_ASCII_70.charAt(ascii_pos);
	}

	/**
	 * Metoda zwraca dwuwymiarową tablicę znaków ASCII mając dwuwymiarową
	 * tablicę odcieni szarości. Metoda iteruje po elementach tablicy odcieni
	 * szarości i dla każdego elementu wywołuje {@ref #intensityToAscii(int)}.
	 * 
	 * @param intensities
	 *            tablica odcieni szarości obrazu
	 * @return tablica znaków ASCII
	 */
	public static char[][] intensitiesToAscii(int[][] intensities, int max_intensity) {
		int rows = intensities.length;
		int columns = intensities[0].length;
		char[][] ascii = new char [rows][columns];
		IntStream.range(0,  rows).forEach(
				i -> IntStream.range(0, columns).forEach(
					j -> ascii[i][j] = intensityToAscii(intensities[i][j], max_intensity)
				)
			);
		
		return ascii;
	}
	
	public static char[][] intensitiesToAscii_hq(int[][] intensities, int max_intensity) {
		int rows = intensities.length;
		int columns = intensities[0].length;
		char[][] ascii = new char [rows][columns];

		IntStream.range(0,  rows).forEach(
			i -> IntStream.range(0, columns).forEach(
				j -> ascii[i][j] = intensityToAscii_hq(intensities[i][j], max_intensity)
			)
		);
		return ascii;
	}
	
	public static void toGrayscale(BufferedImage img){
        int r, g, b;
		Color color;
		int width = img.getWidth();
	    int height = img.getHeight();
	    for(int i=0; i<width; i++){
	      for(int j=0; j<height; j++){
	        color = new Color(img.getRGB(i, j));
	        r = (int)(color.getRed() * 0.2989);
	        g = (int)(color.getGreen() * 0.5870);
	        b = (int)(color.getBlue() * 0.1140);
	        int sum = r + g + b;
	        Color newColor = new Color(sum,sum,sum);
	        img.setRGB(i,j,newColor.getRGB());
	      }
	    }
	}
	
	public static char[][] buffToAscii(BufferedImage img, boolean hq){
		int width = img.getWidth();
	    int height = img.getHeight();
	    Color color;
	    int intensities[][] = null;
		intensities = new int[height][];
		for (int i = 0; i < height; i++) {
			intensities[i] = new int[width];
		}

	    for(int i=0; i<height; i++){
		      for(int j=0; j<width; j++){
		        color = new Color(img.getRGB(j, i));
		        intensities[i][j] = color.getRed();
		      }
		 }
	    if(hq)
	    	return intensitiesToAscii_hq(intensities, 255);
	    return intensitiesToAscii(intensities, 255);
	}

	public static BufferedImage toBufferedImage(Image img)
	{
	    if (img instanceof BufferedImage)
	    {
	        return (BufferedImage) img;
	    }

	    // Create a buffered image with transparency
	    BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);


	    Graphics2D bGr = bimage.createGraphics();
	    bGr.drawImage(img, 0, 0, null);
	    bGr.dispose();

	    // Return the buffered image
	    return bimage;
	}
}
