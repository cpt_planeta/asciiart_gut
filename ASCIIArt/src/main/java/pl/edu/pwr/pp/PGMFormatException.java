package pl.edu.pwr.pp;

public class PGMFormatException extends Exception {
	private static final long serialVersionUID = 1L;
	
	String message;
	
	public PGMFormatException(String msg){
		message = msg;
	}
	
	public String getMessage(){
		return message;
	}
}
