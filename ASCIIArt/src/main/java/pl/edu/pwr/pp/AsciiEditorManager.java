package pl.edu.pwr.pp;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class AsciiEditorManager {		
	ImageFileReader imgRead = new ImageFileReader();
	ImageFileWriter imgWrite = new ImageFileWriter();
	String imagePath = null;
	BufferedImage currentImage = null;
	BufferedImage toSaveImage = null;
	boolean grayscale;
	boolean highQuality;
	int width;
	
	public void setGrayscale(boolean b){
		grayscale = b;
	}
	
	public void saveImage(String pathToSave){
		    if(!grayscale){
		    	ImageConverter.toGrayscale(currentImage);
		    	grayscale = true;
		    }
		    resize();
		    char[][] asciiTab = ImageConverter.buffToAscii(toSaveImage, highQuality);
		    if (!pathToSave.endsWith(".txt"))
			    pathToSave += ".txt";
			imgWrite.saveToTxtFile(asciiTab, pathToSave);
	}
	
	public void setCurrentImage(BufferedImage img){
		currentImage = img;
	}
	
	public BufferedImage getCurrentImage(){
		return currentImage;
	}
	
	public void setImagePath(String path){
		imagePath = path;
	}
	
	public String getImageFromURL(String url){ 
		File temp;
		try {
			String extension = "";
			int i = url.lastIndexOf('.');
			if (i > 0) {
			    extension = url.substring(i);
			}
			temp = File.createTempFile("asciiartTEMP", extension);
			temp.deleteOnExit();
			try {
				String path = temp.getAbsolutePath();
				ImageFileReader.saveImage(url, path);
				imagePath = path;
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} 

		return imagePath;
	}
	
	public void isImageGrayscale(){
		if(currentImage.getType() == BufferedImage.TYPE_BYTE_GRAY)
			grayscale = true;
		else
			grayscale = false;
	}
	
	public void setQuality(String quality){
		if(quality.equals("Wysoka"))
			highQuality = true;
		else
			highQuality = false;	
	}
	
	public void setWidth(String width_str){
		switch (width_str){
		case "80 znaków":
			width = 80;
			break;
		case "160 znaków":
			width = 160;
			break;
		case "Szerokość ekranu":
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			width = (int) screenSize.getWidth();
			break;
		case "Oryginalna":
			width = currentImage.getWidth();
			break;
		}	
	}
	
	private void resize(){		
		float sourceRatio = (float)currentImage.getWidth()/(float)currentImage.getHeight();
		toSaveImage = ImageConverter.toBufferedImage(
				currentImage.getScaledInstance(width, (int) (width*sourceRatio), Image.SCALE_SMOOTH));
	}
	
}
