package pl.edu.pwr.pp;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.SpringLayout;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JComboBox;

public class AsciiEditor {
	AsciiEditorManager manager = new AsciiEditorManager();
	
	private JFrame frame;
	private JLabel lblImg;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AsciiEditor window = new AsciiEditor();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AsciiEditor() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Ascii Editor");
		frame.setBounds(100, 100, 450, 330);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		ImageFileReader im = new ImageFileReader();
		lblImg = new JLabel(im.getWhiteIcon());
		springLayout.putConstraint(SpringLayout.NORTH, lblImg, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, lblImg, -21, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblImg, -24, SpringLayout.EAST, frame.getContentPane());
		frame.getContentPane().add(lblImg);
		
		String[] qualities = { "Niska", "Wysoka" };
		JComboBox comboBoxQuality = new JComboBox(qualities);
		springLayout.putConstraint(SpringLayout.NORTH, comboBoxQuality, 98, SpringLayout.NORTH, frame.getContentPane());
		frame.getContentPane().add(comboBoxQuality);
		
		String[] sizes = { "80 znaków", "160 znaków", "Szerokość ekranu", "Oryginalna" };
		JComboBox comboBoxSize = new JComboBox(sizes);
		springLayout.putConstraint(SpringLayout.WEST, comboBoxQuality, 0, SpringLayout.WEST, comboBoxSize);
		springLayout.putConstraint(SpringLayout.WEST, comboBoxSize, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, comboBoxSize, -20, SpringLayout.WEST, lblImg);
		frame.getContentPane().add(comboBoxSize);
		
		JButton btnSaveImg = new JButton("Zapisz do pliku");
		springLayout.putConstraint(SpringLayout.EAST, comboBoxQuality, 0, SpringLayout.EAST, btnSaveImg);
		springLayout.putConstraint(SpringLayout.WEST, btnSaveImg, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnSaveImg, -20, SpringLayout.WEST, lblImg);
		btnSaveImg.addActionListener(
			e -> { 
				JFileChooser chooser = new JFileChooser();
			    FileNameExtensionFilter filter = new FileNameExtensionFilter("TXT", "txt");
			    chooser.setFileFilter(filter);
			    int returnVal = chooser.showOpenDialog(frame);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			    	String pathToSave = chooser.getSelectedFile().getAbsolutePath().replace('\\', '/');
			    	manager.setWidth((String)comboBoxSize.getSelectedItem());
			    	manager.setQuality((String)comboBoxQuality.getSelectedItem());
			    	manager.saveImage(pathToSave);
			    	JOptionPane.showMessageDialog(frame, String.format("Zapisano do pliku %s",pathToSave));
			    }});

		springLayout.putConstraint(SpringLayout.SOUTH, btnSaveImg, -78, SpringLayout.SOUTH, frame.getContentPane());
		btnSaveImg.setEnabled(false);
		frame.getContentPane().add(btnSaveImg);
		
		JButton btnLoadImg = new JButton("Wczytaj obraz");
		springLayout.putConstraint(SpringLayout.WEST, btnLoadImg, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, btnLoadImg, -20, SpringLayout.WEST, lblImg);
		btnLoadImg.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				btnLoadImg.setEnabled(false);
				btnLoadImg.setText("Wczytywanie...");
				LoadImgDialog dialog = new LoadImgDialog();
				dialog.setModal(true);
				dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
				dialog.setLocationRelativeTo(frame);
				dialog.setVisible(true);
				String imagePath = dialog.getPath();
				if (imagePath != null) {
					if(imagePath.startsWith("URL")){
						imagePath = imagePath.substring(3);
						imagePath = manager.getImageFromURL(imagePath);
					} 
					manager.setImagePath(imagePath);
					ImageFileReader imgReader = new ImageFileReader();
					manager.setCurrentImage(imgReader.getBufferedImage(imagePath));
					ImageIcon iimg = imgReader.getImageIcon(manager.getCurrentImage(), lblImg.getHeight(), lblImg.getWidth());
					lblImg.setIcon(iimg);
					//if (imagePath.endsWith(".pgm") && imgReader.isValidPgm(imagePath))
					manager.isImageGrayscale();
					btnSaveImg.setEnabled(true);
				}
				btnLoadImg.setEnabled(true);
				btnLoadImg.setText("Wczytaj obraz");
			}
		});
		frame.getContentPane().add(btnLoadImg);
		

		
		JLabel lblJakosc = new JLabel("Jakość");
		springLayout.putConstraint(SpringLayout.SOUTH, btnLoadImg, -14, SpringLayout.NORTH, lblJakosc);
		springLayout.putConstraint(SpringLayout.WEST, lblJakosc, 0, SpringLayout.WEST, comboBoxQuality);
		springLayout.putConstraint(SpringLayout.SOUTH, lblJakosc, -6, SpringLayout.NORTH, comboBoxQuality);
		springLayout.putConstraint(SpringLayout.EAST, lblJakosc, -20, SpringLayout.WEST, lblImg);
		frame.getContentPane().add(lblJakosc);
		
		JLabel lblRozmar = new JLabel("Rozmiar");
		springLayout.putConstraint(SpringLayout.WEST, lblRozmar, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, lblRozmar, -142, SpringLayout.SOUTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, lblRozmar, -20, SpringLayout.WEST, lblImg);
		springLayout.putConstraint(SpringLayout.NORTH, comboBoxSize, 6, SpringLayout.SOUTH, lblRozmar);
		frame.getContentPane().add(lblRozmar);
		
	}
}
