package pl.edu.pwr.pp;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.event.ActionListener;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.awt.event.ActionEvent;
import javax.swing.JRadioButton;
import javax.swing.SpringLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class LoadImgDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final JPanel contentPanel = new JPanel();
	private JRadioButton rdbtnFromDrive;
	private JTextField textFieldURL;
	private JRadioButton rdbtnFromURL;
	private JLabel lblImgPath = new JLabel("");
	private String returnPath = null;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			LoadImgDialog dialog = new LoadImgDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public LoadImgDialog() {		
		setTitle("Wczytaj obraz");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		SpringLayout sl_contentPanel = new SpringLayout();
		contentPanel.setLayout(sl_contentPanel);
		
		Border border = BorderFactory.createLineBorder(Color.BLACK, 1);
		
		{
			rdbtnFromDrive = new JRadioButton("Z dysku");
			rdbtnFromDrive.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					textFieldURL.setEnabled(false);
					textFieldURL.setBackground(Color.LIGHT_GRAY);
					lblImgPath.setBackground(Color.WHITE);
				}
			});
			sl_contentPanel.putConstraint(SpringLayout.NORTH, rdbtnFromDrive, 30, SpringLayout.NORTH, contentPanel);
			sl_contentPanel.putConstraint(SpringLayout.WEST, rdbtnFromDrive, 30, SpringLayout.WEST, contentPanel);
			contentPanel.add(rdbtnFromDrive);
		}
		{
			rdbtnFromURL = new JRadioButton("Z adresu URL");
			rdbtnFromURL.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					textFieldURL.setEnabled(true);
					textFieldURL.setBackground(Color.WHITE);
					lblImgPath.setBackground(Color.LIGHT_GRAY);
				}
			});
			sl_contentPanel.putConstraint(SpringLayout.NORTH, rdbtnFromURL, 47, SpringLayout.SOUTH, rdbtnFromDrive);
			sl_contentPanel.putConstraint(SpringLayout.WEST, rdbtnFromURL, 0, SpringLayout.WEST, rdbtnFromDrive);
			contentPanel.add(rdbtnFromURL);
		}
		ButtonGroup bG = new ButtonGroup();
		bG.add(rdbtnFromDrive);
		bG.add(rdbtnFromURL);
		rdbtnFromDrive.setSelected(true);
		
		lblImgPath = new JLabel("");
		sl_contentPanel.putConstraint(SpringLayout.NORTH, lblImgPath, 6, SpringLayout.SOUTH, rdbtnFromDrive);
		sl_contentPanel.putConstraint(SpringLayout.WEST, lblImgPath, 0, SpringLayout.WEST, rdbtnFromDrive);
		sl_contentPanel.putConstraint(SpringLayout.SOUTH, lblImgPath, 29, SpringLayout.SOUTH, rdbtnFromDrive);
		contentPanel.add(lblImgPath);
		lblImgPath.setBackground(Color.WHITE);
		lblImgPath.setOpaque(true);
		lblImgPath.setBorder(border);
		
		JButton btnWybierzPlik = new JButton("Wybierz plik");
		btnWybierzPlik.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setAcceptAllFileFilterUsed(false);
			    FileNameExtensionFilter filter = new FileNameExtensionFilter(
			        "PGM, JPG, PNG", "jpg", "pgm", "png");
			    chooser.setFileFilter(filter);
			    int returnVal = chooser.showOpenDialog(contentPanel);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			    	lblImgPath.setText(chooser.getSelectedFile().getAbsolutePath().replace('\\', '/'));  	
			    }
			}
		});
		sl_contentPanel.putConstraint(SpringLayout.NORTH, btnWybierzPlik, 0, SpringLayout.NORTH, rdbtnFromDrive);
		sl_contentPanel.putConstraint(SpringLayout.WEST, btnWybierzPlik, 17, SpringLayout.EAST, rdbtnFromDrive);
		contentPanel.add(btnWybierzPlik);
		
		textFieldURL = new JTextField();
		sl_contentPanel.putConstraint(SpringLayout.EAST, lblImgPath, 0, SpringLayout.EAST, textFieldURL);
		sl_contentPanel.putConstraint(SpringLayout.NORTH, textFieldURL, 6, SpringLayout.SOUTH, rdbtnFromURL);
		sl_contentPanel.putConstraint(SpringLayout.WEST, textFieldURL, 0, SpringLayout.WEST, rdbtnFromDrive);
		sl_contentPanel.putConstraint(SpringLayout.SOUTH, textFieldURL, 29, SpringLayout.SOUTH, rdbtnFromURL);
		sl_contentPanel.putConstraint(SpringLayout.EAST, textFieldURL, 380, SpringLayout.WEST, contentPanel);
		contentPanel.add(textFieldURL);
		textFieldURL.setColumns(10);
		textFieldURL.setBorder(border);
		textFieldURL.setEnabled(false);
		textFieldURL.setBackground(Color.LIGHT_GRAY);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						if(rdbtnFromDrive.isSelected()){
							File f = new File(lblImgPath.getText());
							if(f.exists() && !f.isDirectory()) { 
								returnPath = lblImgPath.getText();
								dispose();
							} else if (lblImgPath.getText() == ""){
								JOptionPane.showMessageDialog(contentPanel, "Wybierz obrazek do wczytania!");
							} else {
								JOptionPane.showMessageDialog(contentPanel, "Wybrano niepoprawny plik!");
							}
						}
						else if(rdbtnFromURL.isSelected() && textFieldURL.getText() != ""){
							try {
								URL u = new URL(textFieldURL.getText());
								u.toURI();
								returnPath = textFieldURL.getText();
								returnPath = "URL" + returnPath;
								dispose();
							} catch (MalformedURLException e1) {
								JOptionPane.showMessageDialog(contentPanel, "To nie wyglada na poprawny URL!");
							} catch (URISyntaxException e2) {
								JOptionPane.showMessageDialog(contentPanel, "Problem przy konwersji URL na URI!");
							}
						}
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	public String getPath(){
		return returnPath;
	}
}
