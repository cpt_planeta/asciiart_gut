package pl.edu.pwr.pp;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import eugfc.imageio.plugins.PNMRegistry;

public class ImageFileReader {

	/**
	 * Metoda czyta plik pgm i zwraca tablicę odcieni szarości.
	 * @param fileName nazwa pliku pgm
	 * @return tablica odcieni szarości odczytanych z pliku
	 * @throws URISyntaxException jeżeli plik nie istnieje
	 */
	public int[][] readPgmFileAbsolutePath(String filePath){
		int columns = 0;
		int rows = 0;
		int[][] intensities = null;

		Path path = Paths.get(filePath);
		try {
			if (!isValidPgm(filePath))
				throw new PGMFormatException("PGM corrupted failed.");
		}
		catch (PGMFormatException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		
		try (BufferedReader reader = Files.newBufferedReader(path)) {		
			for (int i=0; i<2; ++i)
				reader.readLine();
			String line = reader.readLine().trim();
			String[] cols_and_rows = line.split("\\s+");
			columns = Integer.parseInt(cols_and_rows[0]);
			rows = Integer.parseInt(cols_and_rows[1]);
			
			reader.readLine();
			// inicjalizacja tablicy na odcienie szarości
			intensities = new int[rows][];

			for (int i = 0; i < rows; i++) {
				intensities[i] = new int[columns];
			}
			
			// kolejne linijki pliku pgm zawierają odcienie szarości kolejnych
			// pikseli rozdzielone spacjami
			line = null;
			int currentRow = 0;
			int currentColumn = 0;
			while ((line = reader.readLine().trim()) != null) {
				String[] elements = line.split("\\s+");
				for (int i = 0; i < elements.length; i++) {
					intensities[currentRow][currentColumn] = Integer.parseInt(elements[i]);
					// currentRow i currentColumn są na początku równe zero.
					// Należy je odpowiednio zwiększać, pamiętając o tym, żeby
					// nie wyjść poza zakres tablicy. Plik pgm może mieć w
					// wierszu dowolną ilość liczb, niekoniecznie równą liczbie
					// kolumn.
					currentColumn++;
					if (currentColumn == columns){
						currentColumn = 0;
						currentRow++;
					}
					if (currentRow == rows)
						break;
				}	
				if (currentRow == rows)
					break;	
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
		} 

		return intensities;
	}
	public int[][] readPgmFile(String fileName) throws URISyntaxException {
		Path path = this.getPathToFile(fileName);
		
		return readPgmFileAbsolutePath(path.toString());	
	}
	
	public BufferedImage getBufferedImage(String filePathOrURL) {
		BufferedImage img = null;
		PNMRegistry.registerAllServicesProviders();
		try {
			File f = new File(filePathOrURL);
			if(f.exists() && !f.isDirectory()) { 
			    img = ImageIO.read(f);
			}
			else {
				URL u = new URL(filePathOrURL);
			    img = ImageIO.read(u);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return img;
	}
	
	public ImageIcon getImageIcon(BufferedImage img, int height, int width){
		Image dimg = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
		ImageIcon icon = new ImageIcon(dimg);
		
		return icon;
	}
	
	
	Path getPathToFile(String fileName) throws URISyntaxException {
		URI uri = ClassLoader.getSystemResource(fileName).toURI();
		return Paths.get(uri);
	}
	
	boolean isValidNumber(String number) {
		for (int i = 0; i < number.length(); i++) 
		    if (!Character.isDigit(number.charAt(i))) 
		    	return false;
		return true;
	}
	
	public static void saveImage(String imageUrl, String destinationFile) throws IOException {
		URL url = new URL(imageUrl);
		InputStream is = url.openStream();
		OutputStream os = new FileOutputStream(destinationFile);

		byte[] b = new byte[2048];
		int length;

		while ((length = is.read(b)) != -1) {
			os.write(b, 0, length);
		}

		is.close();
		os.close();
	}
	
	public ImageIcon getWhiteIcon(){
		BufferedImage img = new BufferedImage(230, 230, BufferedImage.TYPE_INT_RGB);
		Graphics g = img.getGraphics();
		g.setColor(Color.white);
		g.fillRect(0, 0, 230, 230);
		return this.getImageIcon(img, 230, 230);
	}
	
	boolean isValidPgm(String filePath){
		Path path = Paths.get(filePath);
		
		try (BufferedReader reader = Files.newBufferedReader(path)) {
			// pierwsza linijka pliku pgm powinna zawierać P2
			if (!reader.readLine().trim().equals("P2"))
				throw new PGMFormatException("First line is not 'P2'. PGM load failed.");
			
			// druga linijka pliku pgm powinna zawierać komentarz rozpoczynający
			// się od #
			if (!reader.readLine().trim().startsWith("#"))
				throw new PGMFormatException("Second line does not start with '#'. PGM load failed.");
			// trzecia linijka pliku pgm powinna zawierać dwie liczby - liczbę
			// kolumn i liczbę wierszy (w tej kolejności). Te wartości należy
			// przypisać do zmiennych columns i rows.
			String line = reader.readLine().trim();
			String[] cols_and_rows = line.split("\\s+");
			if (cols_and_rows.length != 2)
				throw new PGMFormatException("Third line does not contain two numbers. PGM load failed.");
			if (!isValidNumber(cols_and_rows[0]))
				throw new PGMFormatException("Third line columns string is not a number. PGM load failed.");
			if (!isValidNumber(cols_and_rows[1]))
				throw new PGMFormatException("Third line rows string is not a number. PGM load failed.");
			// czwarta linijka pliku pgm powinna zawierać 255 - najwyższą
			// wartość odcienia szarości w pliku
			line = reader.readLine().trim();
			if (!isValidNumber(line))
				throw new PGMFormatException("Fourth line is not a number. PGM load failed.");
			//if (Integer.parseInt(line) != maxIntensity)
			//	throw new PGMFormatException(String.format("Max intensity is not equal to %d. PGM load failed.", maxIntensity));	
		} catch (IOException e) {
			e.printStackTrace();
		} catch (PGMFormatException e) {
			return false;
		}
		
		return true;
	}
}
