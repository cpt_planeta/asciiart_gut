package pl.edu.pwr.pp;
import java.io.PrintWriter;
import java.io.FileNotFoundException;

public class ImageFileWriter {

	public void saveToTxtFile(char[][] ascii, String fileName) {

		PrintWriter writer = null;
		try {
			writer = new PrintWriter(fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		for(int i=0; i< ascii.length; i++){
		    for(int j=0; j< ascii[i].length; j++){
		        writer.print(ascii[i][j]);
		        if (j == ascii[i].length - 1){
		        	writer.println();
		        }
		    }
		}
		
		writer.flush();
		writer.close();
		
	}
	
}
