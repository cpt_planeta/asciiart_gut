package pl.edu.pwr.pp;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.URISyntaxException;

import javax.swing.ImageIcon;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;


public class ImageFileReaderTest {
	
	ImageFileReader imageReader;
	@Mock private BufferedImage img;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		imageReader = new ImageFileReader();
	}

	@Test
	public void shouldReadSequenceFrom0To255GivenTestImage() {
		// given
		String fileName = "testImage.pgm";
		// when
		int[][] intensities = null;
	
			try {
				intensities = imageReader.readPgmFile(fileName);
			} catch (URISyntaxException e) {
				e.printStackTrace();
			}

		// then
		int counter = 0;
		for (int[] row : intensities) {
			for (int intensity : row) {
				assertThat(intensity, is(equalTo(counter++)));
			}
		}
	}
	
	@Test
	public void shouldThrowExceptionWhenFileDontExist() {
		// given
		String fileName = "nonexistent.pgm";
		try {
			// when
			imageReader.readPgmFile(fileName);
			// then
			Assert.fail("Should throw exception");
		} catch (Exception e) {
			assertThat(e, is(instanceOf(NullPointerException.class)));
		}
	}
	
	@Test
	public void shouldReturnNullFileDontExist() {
		// given
		String fileName = "nonexistent.pgm";

		BufferedImage ret = imageReader.getBufferedImage(fileName);

		Assert.assertNull(ret);
	}
	
	@Test
	public void shouldReturnTrueValidNumber() {
		String number = "1000123";
		Assert.assertTrue(imageReader.isValidNumber(number));
	}
	
	@Test
	public void shouldReturnFalseNotValidNumber() {
		String number = "1000   123";
		Assert.assertFalse(imageReader.isValidNumber(number));
		number = "100x23";
		Assert.assertFalse(imageReader.isValidNumber(number));
	}
	
	@Test
	public void shouldThrowExceptionWhenUrlAndFilePathDontExist() {
		// given
		String url = "nonexistent";
		String filePath = "nonexistent";
		try {
			// when
			ImageFileReader.saveImage(url, filePath);
			// then
			Assert.fail("Should throw exception");
		} catch (Exception e) {
			assertThat(e, is(instanceOf(IOException.class)));
		}
	}
	
	
	@Test
	public void shouldReturnFalseWhenPGMisCorrupted() {
		// given
		String path = null;
		String path2 = null;
		String path3 = null;
		String path4 = null;
		try {
			path = imageReader.getPathToFile("testImage_corrupted.pgm").toString();
			path2 = imageReader.getPathToFile("testImage_corrupted2.pgm").toString();
			path3 = imageReader.getPathToFile("testImage_corrupted3.pgm").toString();
			path4 = imageReader.getPathToFile("testImage_corrupted4.pgm").toString();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		
		Assert.assertFalse(imageReader.isValidPgm(path));
		Assert.assertFalse(imageReader.isValidPgm(path2));
		Assert.assertFalse(imageReader.isValidPgm(path3));
		Assert.assertFalse(imageReader.isValidPgm(path4));
	}
	
	@Test
	public void shouldReturnImageIcon() {
		int width = 100;
		int height = 80;
		Mockito.when(img.getScaledInstance(width, height, Image.SCALE_SMOOTH)).thenReturn(new BufferedImage(width,height,BufferedImage.TYPE_INT_RGB));
		ImageIcon result = imageReader.getImageIcon(img, height, width);
		Mockito.verify(img).getScaledInstance(width, height, Image.SCALE_SMOOTH);
		assertThat(result, is(instanceOf(ImageIcon.class)));
	}
}
